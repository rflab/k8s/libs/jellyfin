{
  _config:: {
    name: "jellyfin",
    image: "jellyfin/jellyfin",
    port: 8096,
    domain: '%s.example.com' % self.name,
    config: {},
    tlsSecret: '%s-cert' % self.name,
    storage: {
      cache: {
        class: "",
        size: "",
      },
      config: {
        class: "",
        size: "",
      },
      transcode: {
        class: "",
        size: "",
      },
      media:{
        name: "media",
        path: "",
      },
    }
  },
  local k = import "ksonnet-util/kausal.libsonnet",
  local c = $._config,
  local statefulSet = k.apps.v1.statefulSet,
  local container = k.core.v1.container,
  local port = k.core.v1.containerPort,
  local volumeMount = k.core.v1.volumeMount,
  local service = k.core.v1.service,

  jellyfin: {
    statefulSet: statefulSet.new(
      c.name,
      replicas=1,
      containers=[
        container.new(c.name, c.image)
        + container.withPorts([port.newNamed(c.port, c.name)])
        + container.withVolumeMounts([
          volumeMount.new(self.volumes.config.metadata.name, "/config"),
          volumeMount.new(self.volumes.cache.metadata.name, "/cache"),
          volumeMount.new(self.volumes.transcode.metadata.name, "/transcode"),
          volumeMount.new(c.storage.media.name, "/media"),
        ])
      ],
      volumeClaims=[self.volumes.config, self.volumes.cache, self.volumes.transcode],
      podLabels={"app": c.name}
    )
    + statefulSet.spec.withServiceName(self.service.metadata.name)
    + statefulSet.spec.template.spec.withVolumes([
      k.core.v1.volume.fromHostPath(c.storage.media.name, c.storage.media.path)
    ]),

    volumes: {
      local pvc = k.core.v1.persistentVolumeClaim,
      local volume = k.core.v1.volume,
      config: pvc.new("config")
      + pvc.spec.withAccessModes("ReadWriteOnce")
      + pvc.spec.withStorageClassName(c.storage.config.class)
      + pvc.spec.resources.withRequests({"storage": c.storage.config.size}),
      cache: pvc.new("cache")
      + pvc.spec.withAccessModes("ReadWriteOnce")
      + pvc.spec.withStorageClassName(c.storage.cache.class)
      + pvc.spec.resources.withRequests({"storage": c.storage.cache.size}),
      transcode: pvc.new("transcode")
      + pvc.spec.withAccessModes("ReadWriteOnce")
      + pvc.spec.withStorageClassName(c.storage.transcode.class)
      + pvc.spec.resources.withRequests({"storage": c.storage.transcode.size}),
    },

    service: k.util.serviceFor(self.statefulSet)
    + service.spec.withType("ClusterIP")
    + service.spec.withSelector({"app": c.name})
    + service.metadata.withName(c.name)
    + service.metadata.withLabels({"app": c.name}),

    local ingress = k.networking.v1.ingress,
    local ingressTLS = k.networking.v1.ingressTLS,
    local ingressRule = k.networking.v1.ingressRule,
    local ingressPath = k.networking.v1.httpIngressPath,
    ingress: ingress.new(c.name)
    + ingress.metadata.withAnnotations({"cert-manager.io/cluster-issuer": "letsencrypt-prod"})
    + ingress.spec.withTls(
      ingressTLS.withHosts(c.domain)
      + ingressTLS.withSecretName(c.tlsSecret)
    )
    + ingress.spec.withRules(
      ingressRule.withHost(c.domain)
      + ingressRule.http.withPaths([
        ingressPath.withPath("/")
        + ingressPath.withPathType("Prefix")
        + ingressPath.backend.service.withName(self.service.metadata.name)
        + ingressPath.backend.service.port.withNumber(self.service.spec.ports[0].port)
        ])
    ),

  }

}
