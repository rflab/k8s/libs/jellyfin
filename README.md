# Jellyfin

Sets up jellyfin.

## Usage:

### Install
```sh
jb install git@gitlab.com:rflab/k8s/libs/jellyfin.git

```

### Minimal configuration

Need to specify storage class and sizes for the various volumes, as well give it
a hostPath to find the media.

``` jsonnet
(import "gitlab.com/rflab/k8s/libs/jellyfin/jellyfin.libsonnet")+
{
  local k = import "ksonnet-util/kausal.libsonnet",
  local c = $._config,
  _config+:: {
    storage+: {
      cache+: {
        class: "hdd",
        size: "10Gi",
      },
      config+: {
        class: "hdd",
        size: "2Gi",
      },
      transcode+: {
        class: "ssd",
        size: "50Gi",
      },
      media+: {
        path: "/Storage/Media",
      }

    },
  },
}
```
